INTRODUCTION
------------

This Ckeditor Nofollow module is used to add rel="nofollow" to links using
cleditor widget.

REQUIREMENTS
------------

This module requires ckeditor modules outside of Drupal core.

INSTALLATION
------------

Install the Ckeditor Nofollow module as you would normally install a
contributed Drupal module.

* Visit https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
--------------
Navigate To >

1 Administration > Extend and enable the ckeditor nowfollow module
2 Administration > Configuration > Content authoring > Text formats and editors

Move ckeditor nofollow icon to active toolbar.
