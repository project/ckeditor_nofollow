<?php

namespace Drupal\ckeditor_nofollow\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Ckeditor Nofollow" plugin.
 *
 * @CKEditorPlugin(
 *   id = "nofollow",
 *   label = @Translation("Ckeditor Nofollow"),
 *   module = "ckeditor_nofollow"
 * )
 */
class Nofollow extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'ckeditor_nofollow') . '/js/plugins/nofollow/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $iconImage = drupal_get_path('module', 'ckeditor_nofollow') . '/js/plugins/nofollow/icons/nofollow.png';
    return [
      'nofollow' => [
        'label' => t('No follow'),
        'image' => $iconImage,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(Editor $editor) {
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

}
