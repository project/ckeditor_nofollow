/*
 *  # Nofollow Plugin for CKEditor #
 *
 *  (C) Copyright Mohit Parashar (all rights reserverd)
 *  MIT License
 *
 *
 */


/*
 * A ckeditor plugin to add no follow button for selected word
 * Use this plugin to extend ckeditor toolbar with the functionality of nofollow.
 */

(function ($, Drupal, CKEDITOR) {

  'use strict';

  CKEDITOR.plugins.add('nofollow', {
    requires: 'widget',
    icons: 'nofollow',
    hidpi: true,
    init: function (editor) {

      editor.addCommand('nofollow', {
        exec: function (editor) {
          var linkElement = getNofolowSelectedLink(editor);
          if (linkElement && linkElement.$) {
            if (linkElement.getAttribute('href')) {
              if (linkElement.hasAttribute('rel')) {
                linkElement.removeAttribute('rel');
              }
              else {
                linkElement.setAttribute('rel', 'nofollow');
              }
            }
          }
        }
      });

      var style = new CKEDITOR.style({element: 'a', attributes: {rel: 'nofollow'}});
      // Listen to contextual style activation.
      editor.attachStyleStateChange(style, function (state) {
        return !editor.readOnly && editor.getCommand('nofollow').setState(state);
      });

      editor.ui.addButton('nofollow', {
        label: Drupal.t('Add nofollow'),
        command: 'nofollow',
        toolbar: 'insert',
        icon: this.path + 'icons/nofollow.png'
      });
    }
  });

  function getNofolowSelectedLink(editor) {
    var selection = editor.getSelection();
    var selectedElement = selection.getSelectedElement();
    if (selectedElement && selectedElement.is('a')) {
      return selectedElement;
    }

    var range = selection.getRanges(true)[0];

    if (range) {
      range.shrink(CKEDITOR.SHRINK_TEXT);
      return editor.elementPath(range.getCommonAncestor()).contains('a', 1);
    }
    return null;
  }

})(jQuery, Drupal, CKEDITOR);
